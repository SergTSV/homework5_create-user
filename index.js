const app = document.getElementById('app')
const inputFirstName = app.querySelector('#firstName');
const inputLastName = app.querySelector('#lastName');
const btnCreateUser = app.querySelector('#createUser');


function createNewUser () {
    const newUser = {
        firstName: '',
        lastName: '',

        getLogin() {
           console.log(`${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`)
        },
        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {writable: true});
            this.firstName = newFirstName;
            Object.defineProperty(this, 'firstName', {writable: false});
        },
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {writable: true});
            this.lastName = newLastName;
            Object.defineProperty(this, 'lastName', {writable: false});
        },
    };
    Object.defineProperties(this, {
            firstName: {writable: false},
            lastName: {writable: false},
        }
    );

        btnCreateUser.addEventListener('click', () => {
            if (inputFirstName.value && inputLastName.value) {
                newUser.setFirstName(inputFirstName.value);
                newUser.setLastName(inputLastName.value);
                newUser.getLogin();
            } else {
                alert('Input Error. Enter the data again');
            }
        });

    return newUser;
}

getNewUser = createNewUser();










